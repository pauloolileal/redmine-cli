﻿using System.Collections.Generic;
using System.Linq;

namespace Redmine.Commons.Extensions {

    public static class ColectionExtension {

        public static IEnumerable<(T item, int index)> WithIndex<T>( this IEnumerable<T> self ) =>
            self?.Select( ( item, index ) => (item, index) ) ?? new List<(T, int)>( );
    }
}