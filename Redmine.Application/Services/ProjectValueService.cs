﻿using Redmine.Application.Models;
using Redmine.Application.Services.Base;
using System.Collections.Generic;

namespace Redmine.Application.Services {

    public class ProjectValueService: Service<ProjectValue> {

        public ProjectValue LoadOrCreate( string projectId ) {
            var project = base.Get( x => x.ProjectId == projectId );
            if ( project == null )
                project = new ProjectValue( projectId );
            return project;
        }

        public ProjectValue AddOrUpdateAssignmentTypes( string projectId, List<string> values ) {
            var allowed = LoadOrCreate( projectId );
            allowed.AssignmentTypes = values;
            allowed.Update( );
            return base.AddOrUpdate( allowed );
        }

        public ProjectValue AddOrUpdateModules( string projectId, List<string> values ) {
            var allowed = LoadOrCreate( projectId );
            allowed.Modules = values;
            allowed.Update( );
            return base.AddOrUpdate( allowed );
        }

        public ProjectValue AddOrUpdatePriority( string projectId, List<string> values ) {
            var allowed = LoadOrCreate( projectId );
            allowed.Priority = values;
            allowed.Update( );
            return base.AddOrUpdate( allowed );
        }

        public ProjectValue AddOrUpdateStatus( string projectId, List<string> values ) {
            var allowed = LoadOrCreate( projectId );
            allowed.Status = values;
            allowed.Update( );
            return base.AddOrUpdate( allowed );
        }

        public ProjectValue AddOrUpdateVersions( string projectId, List<string> values ) {
            var allowed = LoadOrCreate( projectId );
            allowed.Versions = values;
            allowed.Update( );
            return base.AddOrUpdate( allowed );
        }

        public ProjectValue AddOrUpdateTimeEntryType( string projectId, List<string> values ) {
            var allowed = LoadOrCreate( projectId );
            allowed.TimeEntryType = values;
            allowed.Update( );
            return base.AddOrUpdate( allowed );
        }
    }
}