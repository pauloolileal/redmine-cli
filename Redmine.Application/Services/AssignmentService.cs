﻿using LiteDB;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Redmine.Application.Models;
using Redmine.Application.Services.Base;
using Redmine.Commons;
using Redmine.Commons.Extensions;
using ShellProgressBar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Redmine.Application.Services {

    public class AssignmentService: Service<Assignment> {

        public override BsonValue Add( Assignment entity ) {
            using var loader = new ProgressBar( 9, "Enviando demanda...", Settings.Options );

            var chrome = Browser.GetInstance( );
            chrome
                .NavigateTo( $"projects/{entity.Project.ProjectId}/issues/new" );

            loader.Tick( );

            var tipoElement = chrome.Search( "//*[@id=\"issue_tracker_id\"]" );
            chrome.Wait( 500 );
            var assignmentType = new SelectElement( tipoElement );
            if ( !string.IsNullOrEmpty( entity.Type ) )
                assignmentType.SelectByText( entity.Type, true );
            else
                entity.Type = assignmentType.SelectedOption.Text;

            loader.Tick( );

            chrome.Wait( 500 );

            chrome.Search( "//*[@id=\"issue_subject\"]" ).SendKeys( entity.Title );

            loader.Tick( );

            if ( !string.IsNullOrEmpty( entity.Description ) )
                chrome.Search( "//*[@id=\"issue_description\"]" ).SendKeys( entity.Description );

            var priority = new SelectElement( chrome.Search( "//*[@id=\"issue_priority_id\"]" ) );
            if ( !string.IsNullOrEmpty( entity.Priority ) )
                priority.SelectByText( entity.Priority );
            else
                entity.Priority = priority.SelectedOption.Text;

            entity.Status = new SelectElement( chrome.Search( "//*[@id=\"issue_status_id\"]" ) )
                .SelectedOption.Text;

            loader.Tick( );

            if ( entity.ParentId != 0 )
                chrome.Search( "//*[@id=\"issue_parent_issue_id\"]" ).SendKeys( entity.ParentId.ToString( ) );

            if ( !string.IsNullOrEmpty( entity.Version ) ) {
                var version = chrome.SearchAll( "//*[@id=\"issue_fixed_version_id\"]" );
                if ( version.Any( ) )
                    new SelectElement( version.First( ) )
                        .SelectByText( entity.Version );
            }

            var userMember = new UserService( ).GetGlobal( ).Member;

            loader.Tick( );

            var atribuido = chrome.SearchAll( "//*[@id=\"issue_assigned_to_id\"]" );
            if ( atribuido.Any( ) )
                atribuido.First( ).SendKeys( userMember.Name );

            var responsavel = chrome.SearchAll( "//*[@id=\"issue_custom_field_values_2\"]" );
            if ( responsavel.Any( ) )
                new SelectElement( responsavel.First( ) )
                    .SelectByText( userMember.Name );

            var textForVersionInput = chrome.SearchAll( "//*[@id=\"issue_custom_field_values_7\"]" );
            if ( !string.IsNullOrEmpty( entity.TextForVersion ) && textForVersionInput.Any( ) )
                textForVersionInput.First( ).SendKeys( entity.TextForVersion );

            loader.Tick( );

            if ( entity.Modules.Any( ) ) {
                var group = chrome.Driver.FindElement( By.ClassName( "check_box_group" ) );

                var options = group.FindElements( By.TagName( "input" ) );
                foreach ( var module in entity.Modules )
                    options.FirstOrDefault( x => x.GetAttribute( "value" ).Contains( module ) )?.Click( );
            }

            chrome.Search( "//*[@id=\"issue-form\"]/input[3]" ).Submit( );

            loader.Tick( );

            chrome.Wait( 500 );

            var id = chrome.Search( "//*[@id=\"flash_notice\"]/a" ).Text.Replace( "#", "" );

            loader.Tick( );

            entity.AssignmentId = Convert.ToInt64( id );

            loader.Tick( );

            return base.Add( entity );
        }

        public override Assignment Get( Expression<Func<Assignment, bool>> predicate ) {
            return _collection
                .Include( x => x.Project )
                .Include( x => x.Project.AllowedValues )
                .FindOne( predicate );
        }

        public override IEnumerable<Assignment> GetAll( ) {
            return _collection
                .Include( x => x.Project )
                .Include( x => x.Project.AllowedValues )
                .FindAll( );
        }

        public Assignment GetOnline( long issueId ) {
            var chrome = Browser.GetInstance( );

            chrome.NavigateTo( $"issues/{issueId}" );

            var projectId = chrome.Search( "//*[@id=\"quick-search\"]/form/label/a" ).GetAttribute( "href" ).Replace( "https://redmine.tce.mg.gov.br", "" ).Replace( "/projects/", "" ).Replace( "/search", "" );

            var project = new ProjectService( ).Get( x => x.ProjectId == projectId );

            var parents = chrome.Driver.FindElements( By.XPath( "//*[@id=\"content\"]/div[2]/div[2]/div/p/a" ) );

            var parent = parents.Any( ) ? new String( parents.First( ).Text.Where( Char.IsDigit ).ToArray( ) ) : "";

            var title = chrome.Driver.FindElement( By.XPath( "//*[@id=\"content\"]/h2" ) ).Text;

            var version = chrome.Driver.FindElements( By.ClassName( "fixed-version" ) ).FirstOrDefault( )?.FindElement( By.ClassName( "value" ) ).Text ?? "";

            var modules = chrome.Driver.FindElements( By.ClassName( "cf_8" ) ).FirstOrDefault( )?.FindElement( By.ClassName( "value" ) ).Text.Replace( "\"", "" ).Split( ",", StringSplitOptions.RemoveEmptyEntries ).ToList( ) ?? new List<string>( );

            var assigment = new Assignment {
                AssignmentId = Convert.ToInt64( new String( title.Where( Char.IsDigit ).ToArray( ) ) ),
                Type = project.AllowedValues.AssignmentTypes.FirstOrDefault( x => x == string.Concat( title.TakeWhile( ( c ) => c != '#' ) ).Trim( ) ),
                Status = project.AllowedValues.Status.FirstOrDefault( x => x == chrome.Driver.FindElement( By.ClassName( "status" ) ).FindElement( By.ClassName( "value" ) ).Text ),
                Priority = project.AllowedValues.Priority.FirstOrDefault( x => x == chrome.Driver.FindElement( By.ClassName( "priority" ) ).FindElement( By.ClassName( "value" ) ).Text ),
                Percent = Convert.ToInt32( chrome.Driver.FindElement( By.ClassName( "progress" ) ).FindElement( By.ClassName( "percent" ) ).Text.Replace( "%", "" ) ),
                Title = chrome.Driver.FindElement( By.ClassName( "subject" ) ).FindElement( By.TagName( "h3" ) ).Text,
                Version = version,
                Modules = modules,
                ParentId = string.IsNullOrWhiteSpace( parent ) ? 0 : Convert.ToInt64( parent ),
                Project = project
            };

            if ( !assigment.Project.AllowedValues.TimeEntryType.Any( ) || ( assigment.Project.AllowedValues.UpdatedAt.HasValue && ( DateTime.Now - assigment.Project.AllowedValues.UpdatedAt.Value > TimeSpan.FromDays( 7 ) ) ) ) {
                chrome.NavigateTo( $"issues/{assigment.AssignmentId}/time_entries/new" );

                var timeEntryElement = chrome.SearchAll( "//*[@id=\"time_entry_activity_id\"]" );
                if ( timeEntryElement.Any( ) ) {
                    var timeEntrySelect = new SelectElement( timeEntryElement.First( ) );
                    chrome.Wait( 500 );
                    var allowedValues = timeEntrySelect
                        .Options
                        .OrderByDescending( x => x.GetAttribute( "selected" ) )
                        .Select( x => x.Text )
                        .ToList( );

                    chrome.Wait( 500 );

                    allowedValues.RemoveAll( x => x.Contains( "---" ) );

                    chrome.Wait( 500 );

                    new ProjectValueService( )
                        .AddOrUpdateTimeEntryType( assigment.Project.ProjectId, allowedValues.ToList( ) );
                }
            }

            if ( !Exists( assigment ) )
                base.Add( assigment );
            else if ( !project.IsRoot )
                base.Update( assigment );

            return assigment;
        }

        public void Sync( List<Project> projects ) {
            var chrome = Browser.GetInstance( );

            var rootProjects = projects.Where( x => x.IsRoot );

            using var loader = new ProgressBar( rootProjects.Count( ), "Sincronizando demandas dos projetos...", Settings.Options );

            foreach ( var project in rootProjects ) {
                chrome.NavigateTo( $"projects/{project.ProjectId}/issues?utf8=✓&set_filter=1&sort=id%3Adesc&f%5B%5D=updated_on{( project.AssignmentUpdatedAt.HasValue ? "&op%5Bupdated_on%5D=>%3D&v%5Bupdated_on%5D%5B%5D=" + project.AssignmentUpdatedAt.Value.ToString( "yyyy-MM-dd" ) : "" )}&f%5B%5D=&c%5B%5D=parent&c%5B%5D=fixed_version&c%5B%5D=tracker&c%5B%5D=status&c%5B%5D=subject&c%5B%5D=done_ratio&c%5B%5D=cf_8&c%5B%5D=project&c%5B%5D=priority&group_by=&t%5B%5D=estimated_hours&t%5B%5D=&per_page=100" );

                var nextPage = chrome.SearchAll( "//li[contains(@class,\"next\")]/preceding::a[1]" );

                var totalPages = nextPage.Any( ) ? Convert.ToInt32( nextPage.First( ).Text ) : 1;

                using var pagesLoader = loader.Spawn( totalPages, $"Carregando demandas, total de paginas {totalPages}", Settings.ChildOptions );

                for ( int page = 1; page <= totalPages; page++ ) {
                    Sync( project, projects, page, loader );
                    pagesLoader.Tick( $"Carregando demandas, pagina {page} de {totalPages}" );
                }
                loader.Tick( $"Demandas do projeto pai {project.Name} atualizadas" );
            }

            projects.ForEach( project => project.UpdateAssignment( ) );

            new ProjectService( ).Update( projects );
        }

        public void Sync( Project rootProject, List<Project> projects, int page, IProgressBar loader ) {
            var chrome = Browser.GetInstance( );

            chrome.NavigateTo( $"projects/{rootProject.ProjectId}/issues?utf8=✓&set_filter=1&sort=id%3Adesc&f%5B%5D=updated_on{( rootProject.AssignmentUpdatedAt.HasValue ? "&op%5Bupdated_on%5D=>%3D&v%5Bupdated_on%5D%5B%5D=" + rootProject.AssignmentUpdatedAt.Value.ToString( "yyyy-MM-dd" ) : "" )}&f%5B%5D=&c%5B%5D=parent&c%5B%5D=fixed_version&c%5B%5D=tracker&c%5B%5D=status&c%5B%5D=subject&c%5B%5D=done_ratio&c%5B%5D=cf_8&c%5B%5D=project&c%5B%5D=priority&group_by=&t%5B%5D=estimated_hours&t%5B%5D=&page={page}&per_page=100" );

            var assigments = chrome.SearchAll( "//table[contains(@class, 'list')]/tbody/tr" ).ToList( );

            assigments = assigments.Where( x => projects.Select( x => x.Name ).Contains( x.FindElement( By.ClassName( "project" ) ).Text ) ).ToList( );

            var assignmentList = new List<Assignment>( );

            using var assignmentLoader = loader.Spawn( assigments.Count, "Carregando demandas", Settings.ChildOptions );

            foreach ( (var item, var index) in assigments.WithIndex( ) ) {
                var project = projects.FirstOrDefault( x => x.Name == item.FindElement( By.ClassName( "project" ) ).Text );

                var parent = new String( item.FindElement( By.ClassName( "parent" ) ).Text.Where( Char.IsDigit ).ToArray( ) );

                var assigment = new Assignment {
                    AssignmentId = Convert.ToInt64( item.FindElement( By.ClassName( "id" ) ).Text ),
                    Type = project.AllowedValues.AssignmentTypes.FirstOrDefault( x => x == item.FindElement( By.ClassName( "tracker" ) ).Text ),
                    Status = project.AllowedValues.Status.FirstOrDefault( x => x == item.FindElement( By.ClassName( "status" ) ).Text ),
                    Priority = project.AllowedValues.Priority.FirstOrDefault( x => x == item.FindElement( By.ClassName( "priority" ) ).Text ),
                    Percent = Convert.ToInt32( item.FindElement( By.ClassName( "progress" ) ).GetAttribute( "class" ).Replace( "progress", "" ).Replace( "-", "" ).Trim( ) ),
                    Title = item.FindElement( By.ClassName( "subject" ) ).Text,
                    Version = item.FindElement( By.ClassName( "fixed_version" ) ).Text.Replace( project.Name + " - ", "" ),
                    Modules = item.FindElement( By.ClassName( "cf_8" ) ).Text.Replace( "\"", "" ).Split( ",", StringSplitOptions.RemoveEmptyEntries ).ToList( ),
                    ParentId = string.IsNullOrWhiteSpace( parent ) ? 0 : Convert.ToInt64( parent ),
                    Project = project
                };

                assignmentList.Add( assigment );

                assignmentLoader.Tick( $"Atualizando demanda {assigment.Title}" );
            }

            using var valuesLoader = loader.Spawn( assignmentList.Count, "Carregando informações", Settings.ChildOptions );

            foreach ( (var assigment, var index) in assignmentList.WithIndex( ) ) {
                if ( !assigment.Project.AllowedValues.TimeEntryType.Any( ) || ( assigment.Project.AllowedValues.UpdatedAt.HasValue && ( DateTime.Now - assigment.Project.AllowedValues.UpdatedAt.Value > TimeSpan.FromDays( 7 ) ) ) ) {
                    chrome.NavigateTo( $"issues/{assigment.AssignmentId}/time_entries/new" );

                    var timeEntryElement = chrome.SearchAll( "//*[@id=\"time_entry_activity_id\"]" );
                    if ( timeEntryElement.Any( ) ) {
                        var timeEntrySelect = new SelectElement( timeEntryElement.First( ) );
                        chrome.Wait( 500 );
                        var allowedValues = timeEntrySelect
                            .Options
                            .OrderByDescending( x => x.GetAttribute( "selected" ) )
                            .Select( x => x.Text )
                            .ToList( );

                        chrome.Wait( 500 );

                        allowedValues.RemoveAll( x => x.Contains( "---" ) );

                        chrome.Wait( 500 );

                        new ProjectValueService( )
                            .AddOrUpdateTimeEntryType( assigment.Project.ProjectId, allowedValues.ToList( ) );
                    }
                }

                if ( !Exists( assigment ) )
                    base.Add( assigment );
                else if ( !assigment.Project.IsRoot )
                    base.Update( assigment );

                valuesLoader.Tick( $"Atualizado informações de {assigment.Title}" );
            }
        }

        public override BsonValue Update( Assignment entity ) {
            using var loader = new ProgressBar( 10, $"Atualizando demanda {entity.AssignmentId} - {entity.Title}...", Settings.Options );

            var chrome = Browser.GetInstance( );

            chrome.NavigateTo( $"issues/{entity.AssignmentId}/edit" );

            loader.Tick( );

            var assignmentTypeSelect = new SelectElement( chrome.Search( "//*[@id=\"issue_tracker_id\"]" ) );
            if ( assignmentTypeSelect.SelectedOption.Text != entity.Type )
                assignmentTypeSelect.SelectByText( entity.Type, true );

            loader.Tick( );

            var title = chrome.Search( "//*[@id=\"issue_subject\"]" );
            if ( title.GetAttribute( "value" ) != entity.Title ) {
                title.Clear( );
                title.SendKeys( entity.Title );
            }

            loader.Tick( );

            var priority = chrome.SearchAll( "//*[@id=\"issue_priority_id\"]" );
            if ( priority.Any( ) ) {
                var prioritySelect = new SelectElement( priority.First( ) );
                if ( entity.Priority != prioritySelect.SelectedOption.Text )
                    prioritySelect.SelectByText( entity.Priority );
            }

            var statusSelect = new SelectElement( chrome.Search( "//*[@id=\"issue_status_id\"]" ) );
            var availableOptions = statusSelect.Options.Select( x => x.Text ).ToList( );
            if ( statusSelect.SelectedOption.Text != entity.Status )
                if ( availableOptions.Contains( entity.Status ) ) {
                    statusSelect.SelectByText( entity.Status );
                } else
                    throw new Exception( $"Esse status ainda não pode ser aplicado nessa demanda! As situações permitidas nesse momendo são: {string.Join( ", ", availableOptions )}" );

            loader.Tick( );

            var version = chrome.SearchAll( "//*[@id=\"issue_fixed_version_id\"]" );
            if ( version.Any( ) ) {
                var versionSelect = new SelectElement( version.First( ) );
                if ( !string.IsNullOrEmpty( entity.Version ) && entity.Version != versionSelect.SelectedOption.Text )
                    try {
                        versionSelect.SelectByText( entity.Version );
                    } catch ( Exception ) {
                        versionSelect.SelectByIndex( 0 );
                    }
            }

            loader.Tick( );

            if ( entity.Modules.Any( ) ) {
                var group = chrome.Driver.FindElement( By.ClassName( "check_box_group" ) );

                var options = group.FindElements( By.TagName( "input" ) );
                foreach ( var module in entity.Modules ) {
                    var option = options.FirstOrDefault( x => x.GetAttribute( "value" ).Contains( module ) );
                    if ( option?.GetAttribute( "checked" ) != "true" )
                        option?.Click( );
                }
            }

            var userMember = new UserService( ).GetGlobal( ).Member;
            var responsavel = chrome.Search( "//*[@id=\"issue_custom_field_values_2\"]" );
            if ( responsavel != null ) {
                new SelectElement( responsavel )
                    .SelectByText( userMember.Name );
            }

            var atribuido = chrome.Search( "//*[@id=\"issue_assigned_to_id\"]" );
            if ( atribuido != null )
                atribuido.SendKeys( userMember.Name );

            var percent = chrome.Search( "//*[@id=\"issue_done_ratio\"]" );
            if ( percent != null ) {
                var percentSelect = new SelectElement( percent );
                if ( percentSelect.SelectedOption.Text != entity.Percent.ToString( ) )
                    percentSelect.SelectByValue( entity.Percent.ToString( ) );
            }

            loader.Tick( );

            chrome.Search( "//*[@id=\"issue-form\"]/input[6]" ).Click( );

            loader.Tick( );

            return base.Update( entity );
        }
    }
}