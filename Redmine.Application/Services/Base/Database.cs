﻿using LiteDB;
using Redmine.Commons;

namespace Redmine.Application.Services.Base {

    public class Database {

        public static LiteDatabase DB =>
            _db ??= _db = new LiteDatabase( Settings.ConnectionString );

        private static LiteDatabase _db;
    }
}