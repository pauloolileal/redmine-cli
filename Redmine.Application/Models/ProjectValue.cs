﻿using LiteDB;
using System;
using System.Collections.Generic;

namespace Redmine.Application.Models {

    public class ProjectValue {

        [BsonId( false )]
        public string ProjectId { get; set; }

        public List<string> AssignmentTypes { get; set; } = new List<string>( );
        public List<string> Modules { get; set; } = new List<string>( );
        public List<string> Priority { get; set; } = new List<string>( );
        public List<string> Status { get; set; } = new List<string>( );
        public List<string> Versions { get; set; } = new List<string>( );
        public List<string> TimeEntryType { get; set; } = new List<string>( );

        public DateTime? UpdatedAt { get; set; }

        public ProjectValue( string projectId ) {
            ProjectId = projectId;
        }

        public void Update( ) {
            UpdatedAt = DateTime.Now;
        }
    }
}