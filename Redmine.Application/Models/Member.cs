﻿namespace Redmine.Application.Models {

    public class Member {
        public long MemberId { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }

        public Member( string name, string role ) {
            Name = name;
            Role = role;
        }
    }
}