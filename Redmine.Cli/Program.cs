﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application;
using Redmine.Application.Services;
using Redmine.Cli.Commands.AssignmentCommands;
using Redmine.Cli.Commands.MembemCommands;
using Redmine.Cli.Commands.ProjectCommands;
using Redmine.Cli.Commands.TimeEntryCommands;
using Redmine.Cli.Commands.UserCommands;
using Redmine.Cli.Extensions;
using Redmine.Commons;
using System;
using System.IO;
using System.Linq;

namespace Redmine.Cli {

    [Command( UnrecognizedArgumentHandling = UnrecognizedArgumentHandling.Throw )]
    [Subcommand(
        typeof( AssignmentBase ),
        typeof( ProjectBase ),
        typeof( UserBase ),
        typeof( LoginUser ),
        typeof( MemberBase ),
        typeof( TimeEntryBase ),
        typeof( SyncGlobal )
    )]
    [HelpOption( )]
    public class Program {

        public static int Main( string[ ] args ) {
            try {
                BuildSettings( );
                return CommandLineApplication.Execute<Program>( args );
            } catch ( Exception e ) {
                Writer.Error( "Error: " + e.Message );
            } finally {
                Browser.Close( );
            }

            return -1;
        }

        private static void BuildSettings( ) {
            var path = new DirectoryInfo( Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.ApplicationData ), "Redmine" ) );
            if ( !path.Exists )
                path.Create( );

            Settings.ConnectionString = $"Filename={path.FullName}/redmine.litedb;Connection=direct;Log=255;";
            Settings.AppData = path.FullName;

            var globalUser = new UserService( ).Get( x => x.Global );

            Settings.RedmineUrl = globalUser?.RedmineUrl ?? "http://demo.redmine.org/";
        }

        private void OnExecute( CommandLineApplication app ) {
            app.ShowHelp( );
        }

        private void CleanTimeEntry( ) {
            var timeService = new TimeEntryService( );
            var a = timeService.GetAll( );
            var b = a.Where( x => x.DateStart.TimeOfDay == TimeSpan.Zero ).Select( x => x.TimeEntryId ).ToList( );
            timeService.Delete( x => b.Contains( x.TimeEntryId ) );
        }
    }
}