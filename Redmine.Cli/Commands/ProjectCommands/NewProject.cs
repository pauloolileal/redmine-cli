﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Models;
using Redmine.Application.Services;
using Redmine.Cli.Commands.BaseCommands;
using Redmine.Cli.Extensions;
using System.ComponentModel.DataAnnotations;

namespace Redmine.Cli.Commands.ProjectCommands {

    [Command( "new", "n", "add", "a", Description = "Cadastrar um novo projeto (permissão adicional pode ser requerida)" )]
    public class NewProject: CommandWithLogin {

        [Required]
        [Argument( 1, Description = "Descrição breve do projeto" )]
        public string Description { get; set; }

        [Required]
        [Argument( 0, Description = "Nome do projeto" )]
        public string Name { get; set; }

        private readonly ProjectService _projectService;

        public NewProject( ) => _projectService = new ProjectService( );

        public override void OnExecute( ) {
            Writer.Info( $"Cadastrando projeto: {Name}..." );

            var project = new Project {
                Name = Name,
                Description = Description
            };
            //project.GenerateId( );

            _projectService.Add( project );

            Writer.Success( "Projeto criado com sucesso!" );
        }
    }
}