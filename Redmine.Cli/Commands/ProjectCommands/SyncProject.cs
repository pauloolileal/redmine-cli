﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Services;
using Redmine.Cli.Commands.BaseCommands;

namespace Redmine.Cli.Commands.ProjectCommands {

    [Command( "sync", "s", Description = "Sincronizar os projetos locais com o Redmine" )]
    public class SyncProject: CommandWithLogin {

        [Option( CommandOptionType.NoValue, Template = "-a|--all", Description = "Todos os projetos? Os seus projetos e os públicos" )]
        public bool AllProjects { get; set; }

        private readonly ProjectService _projectService;

        public SyncProject( ) {
            _projectService = new ProjectService( );
        }

        public override void OnExecute( ) {
            _projectService.Sync( AllProjects );
        }
    }
}