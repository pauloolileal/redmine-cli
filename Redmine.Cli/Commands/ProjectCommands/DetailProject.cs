﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Services;
using Redmine.Cli.Commands.BaseCommands;
using Redmine.Cli.Extensions;
using System;
using System.ComponentModel.DataAnnotations;

namespace Redmine.Cli.Commands.ProjectCommands {

    [Command( "detail", "d", Description = "Detalhes de um projeto especifico" )]
    public class DetailProject: CommandBase {

        [Required]
        [Argument( 0, Description = "Id do projeto" )]
        public string ProjectId { get; set; }

        private readonly ProjectService _projectService;

        public DetailProject( ) {
            _projectService = new ProjectService( );
        }

        public override void OnExecute( ) {
            var project = _projectService.Get( x => x.ProjectId == ProjectId );

            var format = "{0,30%} │ {1,30%} │ {2,40%}";

            Writer.Title( format, "ID", "NOME", "DESCRIÇÃO" );

            Writer.WriteLine( format, project.ProjectId, project.Name, project.Description );

            Writer.BreakLine( );

            Writer.Divider( );

            Writer.WriteInColor( "PRIORIDADES: ", ConsoleColor.DarkYellow );

            Writer.WriteInColor( string.Join( ", ", project.AllowedValues.Priority ), ConsoleColor.White );

            Writer.BreakLine( );

            Writer.Divider( );

            Writer.WriteInColor( "SITUAÇÕES: ", ConsoleColor.DarkYellow );

            Writer.WriteInColor( string.Join( ", ", project.AllowedValues.Status ), ConsoleColor.White );

            Writer.BreakLine( );

            Writer.Divider( );

            Writer.WriteInColor( "TIPOS DE DEMANDAS: ", ConsoleColor.DarkYellow );

            Writer.WriteInColor( string.Join( ", ", project.AllowedValues.AssignmentTypes ), ConsoleColor.White );

            Writer.BreakLine( );

            Writer.Divider( );
        }
    }
}