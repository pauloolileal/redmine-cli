﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Services;
using Redmine.Cli.Commands.BaseCommands;
using Redmine.Cli.Extensions;
using System.Linq;

namespace Redmine.Cli.Commands.ProjectCommands {

    [Command( "list", "l", Description = "Listar os projetos" )]
    public class ListProject: CommandBase {
        private readonly ProjectService _projectService;

        public ListProject( ) => _projectService = new ProjectService( );

        public override void OnExecute( ) {
            var projects = _projectService.GetAll( )
                .GroupBy( x => x.ParentId )
                .ToList( );

            var root = projects.FirstOrDefault( x => string.IsNullOrEmpty( x.Key ) );

            projects.RemoveAll( x => string.IsNullOrEmpty( x.Key ) );

            var format = "{0,-10%} │ {1,20%} │ {2,70%}";

            Writer.Title( format, "ID", "NOME", "DESCRIÇÃO" );

            foreach ( var projectGroup in projects ) {
                var parent = root.FirstOrDefault( x => x.ProjectId == projectGroup.Key );
                Writer.WriteLine( format, parent.ProjectId, parent.Name, parent.Description );
                foreach ( var item in projectGroup ) {
                    var glyph = ( item == projectGroup.Last( ) ? "└─ " : "├─ " );
                    Writer.WriteLine( format, glyph + item.ProjectId, item.Name, item.Description );
                }
                Writer.Divider( );
            }
        }
    }
}