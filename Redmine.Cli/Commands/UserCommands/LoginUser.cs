﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Models;
using Redmine.Application.Services;
using Redmine.Cli.Commands.BaseCommands;
using Redmine.Commons;

namespace Redmine.Cli.Commands.UserCommands {

    [Command( "login", "l", Description = "Salva as informações de login e as valida no Redmine" )]
    public class LoginUser: CommandBase {
        private readonly UserService _userService;
        private readonly MemberService _memberService;

        public LoginUser( ) {
            _userService = new UserService( );
            _memberService = new MemberService( );
        }

        public override void OnExecute( ) {
            var url = Prompt.GetString( "Redmine Url: " );
            var username = Prompt.GetString( "Username: " );
            var password = Prompt.GetPassword( "Password: " );

            if ( !url.EndsWith( '/' ) )
                url += "/";

            _userService.DeleteAll( );

            var user = new User( username, password, url, true );

            Settings.RedmineUrl = url;

            _userService.Add( user );

            _userService.Login( );

            user.Member = _memberService.GetMemberForCurrentUser( );

            _userService.Update( user );
        }
    }
}