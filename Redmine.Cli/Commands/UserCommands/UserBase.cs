﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Cli.Commands.BaseCommands;

namespace Redmine.Cli.Commands.UserCommands {

    [Command( "user", "u", Description = "Comandos disponiveis para usuário atual" )]
    [Subcommand(
        typeof( LoginUser )
        )]
    public class UserBase: CommandBase {
    }
}