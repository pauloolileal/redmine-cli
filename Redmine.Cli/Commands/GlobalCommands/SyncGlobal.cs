﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Models;
using Redmine.Application.Services;
using Redmine.Cli.Commands.BaseCommands;
using Redmine.Cli.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Redmine.Cli.Commands.ProjectCommands {

    [Command( "sync", "s", Description = "Sincronizar todas as informaçoes de : Projetos, Demandas e Membros" )]
    public class SyncGlobal: CommandWithLogin {

        [Option( CommandOptionType.NoValue, Template = "-a|--all", Description = "Todos os projetos? Os seus projetos e os públicos" )]
        public bool AllProjects { get; set; }

        [Option( CommandOptionType.SingleValue, Template = "-d|--date", Description = "Atualizar a partir de quando, formato dd/MM/yyyy" )]
        public string Data { get; set; }

        private readonly AssignmentService _assignmentService;
        private readonly MemberService _memberService;
        private readonly ProjectService _projectService;
        private readonly TimeEntryService _timeEntryService;
        private readonly UserService _userService;

        public SyncGlobal( ) {
            _projectService = new ProjectService( );
            _assignmentService = new AssignmentService( );
            _memberService = new MemberService( );
            _timeEntryService = new TimeEntryService( );
            _userService = new UserService( );
        }

        public override void OnExecute( ) {
            DateTime? date = null;
            if ( !string.IsNullOrEmpty( Data ) ) {
                try {
                    date = DateTime.ParseExact( Data, "dd/MM/yyyy", CultureInfo.InvariantCulture );
                } catch ( Exception ) {
                    throw new Exception( "Erro ao fazer parse da data, o formato deve ser: dd/MM/yyyy" );
                }
            }

            Writer.Info( "Iniciando sincronização de dados...\n" );
            var start = DateTime.Now;
            SyncProjects( );

            var projects = _projectService.GetAll( );

            if ( date.HasValue ) {
                var project = projects.ToList( );
                project.ForEach( p => {
                    p.TimeEntryUpdatedAt = date;
                    p.AssignmentUpdatedAt = date;
                } );
                projects = project;
            }

            SyncAssignments( projects );

            SyncTimeEntries( projects );

            SyncMembers( projects );

            var stop = DateTime.Now;
            Writer.Success( "Sincronização efetuada com sucesso! - " + ( stop - start ).ToString( "hh\\:mm" ) );
        }

        private void SyncAssignments( IEnumerable<Project> projects ) {
            _assignmentService.Sync( projects.ToList( ) );
        }

        private void SyncMembers( IEnumerable<Project> projects ) {
            _memberService.Sync( projects );
        }

        private void SyncProjects( ) {
            _projectService.Sync( AllProjects );
        }

        private void SyncTimeEntries( IEnumerable<Project> projects ) {
            _timeEntryService.Sync( projects );
        }
    }
}