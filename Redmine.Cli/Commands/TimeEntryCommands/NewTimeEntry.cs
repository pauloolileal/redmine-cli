﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Models;
using Redmine.Application.Services;
using Redmine.Cli.Commands.BaseCommands;
using Redmine.Cli.Extensions;
using System;
using System.Globalization;
using System.Linq;

namespace Redmine.Cli.Commands.TimeEntryCommands {

    [Command( "new", "n", "add", "a", Description = "Cadastrar um periodo de trabalho fora de hora" )]
    public class NewTimeEntry: CommandBase {

        [Argument( 0, Name = "IssueId", Description = "Id da demanda" )]
        public long AssignmentId { get; set; }

        [Option( CommandOptionType.SingleValue, Description = "Comentários" )]
        public string Comments { get; set; }

        [Argument( 2, Description = "Data e horario de encerramento: dd/MM/yyyy HH:mm ou o tempo de trabalho: HH:mm" )]
        public string End { get; set; }

        [Argument( 1, Description = "Data e horario de inicio: dd/MM/yyyy HH:mm" )]
        public string Start { get; set; }

        [Option( CommandOptionType.SingleValue, Description = "Tipo de atividade" )]
        public string Type { get; set; }

        private readonly AssignmentService _assignmentService;
        private readonly TimeEntryService _timeEntryService;

        public NewTimeEntry( ) {
            _timeEntryService = new TimeEntryService( );
            _assignmentService = new AssignmentService( );
        }

        public override void OnExecute( ) {
            var assignment = _assignmentService.Get( x => x.AssignmentId == AssignmentId );

            if ( assignment == null ) {
                EnsureLoged( );
                assignment = _assignmentService.GetOnline( AssignmentId );
            }

            if ( assignment == null )
                throw new Exception( "Demanda não existe!" );

            var type = assignment.Project.AllowedValues.TimeEntryType.FirstOrDefault( );
            if ( !string.IsNullOrEmpty( Type ) ) {
                type = assignment.Project.AllowedValues.TimeEntryType.FirstOrDefault( x => x.ToLower( ).Contains( Type.ToLower( ) ) );
                if ( string.IsNullOrEmpty( type ) )
                    throw new Exception( $"Esse tipo não é aceito nesse projeto. Os valores aceitos são: {string.Join( ',', assignment.Project.AllowedValues.AssignmentTypes )}" );
            }

            var start = DateTime.Now;
            if ( !string.IsNullOrEmpty( Start ) ) {
                try {
                    start = DateTime.ParseExact( Start, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture );
                } catch ( Exception ) {
                    throw new Exception( "Erro ao fazer parse da data, o formato deve ser: dd/MM/yyyy HH:mm" );
                }
            }

            var end = DateTime.Now;
            if ( !string.IsNullOrEmpty( End ) ) {
                if ( End.Contains( "/" ) ) {
                    try {
                        end = DateTime.ParseExact( End, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture );
                    } catch ( Exception ) {
                        throw new Exception( "Erro ao fazer parse da data, o formato para data deve ser: dd/MM/yyyy HH:mm" );
                    }
                } else {
                    try {
                        var time = DateTime.ParseExact( End, "HH:mm", CultureInfo.InvariantCulture );
                        end = start + time.TimeOfDay;
                    } catch ( Exception ) {
                        throw new Exception( "Erro ao fazer parse do tempo, o formato para tempo deve ser: HH:mm" );
                    }
                }
            }

            var timeEntry = new TimeEntry( assignment, type, Comments, start, end );
            _timeEntryService.Add( timeEntry );

            Writer.Success( "Tempo de trabalho adicionado com sucesso!" );
        }
    }
}