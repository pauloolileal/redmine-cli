﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Services;
using Redmine.Cli.Commands.BaseCommands;
using Redmine.Cli.Extensions;
using System;
using System.Globalization;
using System.Linq;

namespace Redmine.Cli.Commands.TimeEntryCommands {

    [Command( "edit", "ed", "update", "up", Description = "Editar periodo de trabalho" )]
    public class EditTimeEntry: CommandBase {

        [Argument( 0, Name = "TimeEntryId", Description = "Id do periodo de trabalho" )]
        public long TimeId { get; set; }

        [Option( CommandOptionType.SingleValue, Description = "Comentarios" )]
        public string Comments { get; set; }

        [Option( CommandOptionType.SingleValue, Description = "Horario de inicio: HH:mm" )]
        public string Start { get; set; }

        [Option( CommandOptionType.SingleValue, Description = "Horario de finalizar: HH:mm" )]
        public string End { get; set; }

        [Option( CommandOptionType.SingleValue, Description = "Tipo de atividade" )]
        public string Type { get; set; }

        private readonly TimeEntryService _timeEntryService;

        public EditTimeEntry( ) {
            _timeEntryService = new TimeEntryService( );
        }

        public override void OnExecute( ) {
            var timeEntry = _timeEntryService.Get( x => x.TimeEntryId == TimeId );
            if ( timeEntry == null )
                throw new Exception( "Periodo de trabalho não existe!" );

            if ( timeEntry.Submited )
                throw new Exception( "Periodo de trabalho já foi enviado e não pode ser editado!" );

            var assingment = timeEntry.Assignment;

            var activityType = assingment.Project.AllowedValues.TimeEntryType.FirstOrDefault( );
            if ( !string.IsNullOrEmpty( Type ) ) {
                activityType = assingment.Project.AllowedValues.TimeEntryType.FirstOrDefault( x => x.ToLower( ).Contains( Type.ToLower( ) ) );
                if ( string.IsNullOrEmpty( activityType ) )
                    throw new Exception( $"Esse tipo não é aceito nesse projeto. Os valores aceitos são: {string.Join( ',', assingment.Project.AllowedValues.AssignmentTypes )}" );
            }

            var start = DateTime.MinValue;
            if ( !string.IsNullOrEmpty( Start ) ) {
                try {
                    var hour = DateTime.ParseExact( Start, "HH:mm", CultureInfo.InvariantCulture );
                    start = DateTime.Today;
                    start = start.Date + hour.TimeOfDay;
                } catch ( Exception ) {
                    throw new Exception( "Erro ao fazer parse da data, o formato deve ser: HH:mm" );
                }
            }

            var end = DateTime.MinValue;
            if ( !string.IsNullOrEmpty( End ) ) {
                try {
                    var hour = DateTime.ParseExact( End, "HH:mm", CultureInfo.InvariantCulture );
                    end = DateTime.Today;
                    end = end.Date + hour.TimeOfDay;
                } catch ( Exception ) {
                    throw new Exception( "Erro ao fazer parse da data, o formato deve ser: HH:mm" );
                }
            }

            if ( !string.IsNullOrEmpty( activityType ) && timeEntry.Activity != activityType )
                timeEntry.Activity = activityType;

            if ( start != DateTime.MinValue && timeEntry.DateStart != start )
                timeEntry.DateStart = start;

            if ( end != DateTime.MinValue && timeEntry.DateEnd != end )
                timeEntry.DateEnd = end;

            if ( !string.IsNullOrEmpty( Comments ) && timeEntry.Comments != Comments )
                timeEntry.Comments = Comments;

            _timeEntryService.Update( timeEntry );

            Writer.Success( "Periodo de trabalho editado com sucesso!" );
        }
    }
}