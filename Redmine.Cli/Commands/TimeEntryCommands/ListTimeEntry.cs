﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Models;
using Redmine.Application.Services;
using Redmine.Cli.Commands.BaseCommands;
using Redmine.Cli.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Redmine.Cli.Commands.TimeEntryCommands {

    [Command( "list", "l", Description = "Listar os periodos de trabalho exercidos" )]
    public class ListTimeEntry: CommandBase {

        [Option( CommandOptionType.MultipleValue, Template = "-i|--issue", Description = "Enviar tempo gasto nessas demandas" )]
        public IEnumerable<long> Assignments { get; set; } = new List<long>( );

        [Option( CommandOptionType.SingleValue, Template = "-e|--end", Description = "Até esse dia: dd/MM/yyyy" )]
        public string DayEnd { get; set; }

        [Option( CommandOptionType.SingleValue, Template = "-s|--start", Description = "A partir desse dia: dd/MM/yyyy" )]
        public string DayStart { get; set; }

        [Option( CommandOptionType.MultipleValue, Template = "-p|--project", Description = "Enviar tempo gasto nesses projetos" )]
        public IEnumerable<string> Projects { get; set; } = new List<string>( );

        [Option( CommandOptionType.NoValue, Template = "-sub|--submited", Description = "Exibir os periodos já enviados" )]
        public bool Submited { get; set; }

        [Option( CommandOptionType.MultipleValue, Template = "-t|--time", Description = "Enviar esses tempos gastos" )]
        public IEnumerable<long> TimeEntries { get; set; } = new List<long>( );

        private readonly TimeEntryService _timeEntryService;

        public ListTimeEntry( ) {
            _timeEntryService = new TimeEntryService( );
        }

        public override void OnExecute( ) {
            var times = new List<TimeEntry>( );

            if ( Projects.Any( ) )
                times.AddRange( _timeEntryService.GetAll( x => Projects.Contains( x.Assignment.Project.ProjectId ) ) );

            if ( Assignments.Any( ) )
                times.AddRange( _timeEntryService.GetAll( x => Assignments.Contains( x.Assignment.AssignmentId ) ) );

            if ( TimeEntries.Any( ) )
                times.AddRange( _timeEntryService.GetAll( x => TimeEntries.Contains( x.TimeEntryId ) ) );

            if ( !Projects.Any( ) && !Assignments.Any( ) && !TimeEntries.Any( ) )
                times.AddRange( _timeEntryService.GetAll( ) );

            if ( !string.IsNullOrEmpty( DayStart ) ) {
                try {
                    var start = DateTime.ParseExact( DayStart, "dd/MM/yyyy", CultureInfo.InvariantCulture );
                    times = times.Where( x => x.DateStart >= start ).ToList( );
                } catch ( Exception ) {
                    throw new Exception( "Erro ao fazer parse da data, o formato deve ser: dd/MM/yyyy" );
                }
            }

            if ( !string.IsNullOrEmpty( DayEnd ) ) {
                try {
                    var end = DateTime.ParseExact( DayEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture );
                    times = times.Where( x => x.DateEnd <= end ).ToList( );
                } catch ( Exception ) {
                    throw new Exception( "Erro ao fazer parse da data, o formato deve ser: dd/MM/yyyy" );
                }
            }

            if ( !Submited )
                times = times
                    .Where( x => x.Submited == false )
                    .ToList( );

            times
                .GroupBy( x => new { x.TimeEntryId } )
                .Select( g => g.First( ) )
                .OrderBy( x => x.DateStart )
                .ToList( );

            var format = "{0,5%} │ {1,10%} │ {2,5%} │ {3,30%} │ {4,10%} │ {5,20%} | {6,5%} | {7,5%} │ {8,5%} │ {9,5%}";

            Writer.Title( format, "ID", "PROJETO", "DEMANDA", "TITULO", "TIPO", "COMENTARIOS", "INICIO", "FIM", "ENVIADO", "TEMPO" );

            foreach ( var timeGroup in times.GroupBy( x => x.DateStart.Date ).OrderBy( x => x.Key ) ) {
                Writer.WriteLine( "{0,-50%} {1,50%}",
                    "DATA: " + timeGroup.Key.ToString( "dd/MM/yyyy" ),
                    "TEMPO TOTAL: " + TimeSpan.FromMilliseconds( timeGroup.Sum( x => x.Time.TotalMilliseconds ) ).ToString( @"hh\:mm" ) );
                Writer.Divider( );

                foreach ( var time in timeGroup ) {
                    var glyph = ( time == timeGroup.Last( ) ? "└─" : "├─" ).PadRight( 6 - time.TimeEntryId.ToString( ).Length, ' ' );
                    Writer.WriteLine( format,
                        glyph + time.TimeEntryId,
                        time.Assignment.Project.Name,
                        time.Assignment.AssignmentId.ToString( ),
                        time.Assignment.Title,
                        string.IsNullOrEmpty( time.Activity ) ? time.Assignment.Project.AllowedValues.TimeEntryType.FirstOrDefault( ) : time.Activity,
                        time.Comments,
                        time.DateStart.ToString( "HH:mm" ),
                        time.DateEnd.HasValue ? time.DateEnd.Value.ToString( "HH:mm" ) : "",
                        time.Submited ? "SIM" : "NÃO",
                        time.Time.ToString( @"hh\:mm" ) );
                }
                Writer.Divider( );
            }
        }
    }
}