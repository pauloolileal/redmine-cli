﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Services;
using Redmine.Cli.Commands.BaseCommands;
using Redmine.Cli.Extensions;

namespace Redmine.Cli.Commands.MembemCommands {

    [Command( "sync", "s", Description = "Sincronizar os membros locais com o Redmine" )]
    public class SyncMember: CommandWithLogin {
        private readonly MemberService _memberService;
        private readonly ProjectService _projectService;

        public SyncMember( ) {
            _memberService = new MemberService( );
            _projectService = new ProjectService( );
        }

        public override void OnExecute( ) {
            var projects = _projectService.GetAll( );
            _memberService.Sync( projects );

            Writer.Success( "Membros atualizado com sucesso!" );
        }
    }
}