﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Cli.Commands.BaseCommands;

namespace Redmine.Cli.Commands.MembemCommands {

    [Command( "member", "mem", "m", Description = "Comandos disponiveis para membros das equipes" )]
    [Subcommand(
        typeof( SyncMember ),
        typeof( ListMember )
        )]
    public class MemberBase: CommandBase {
    }
}