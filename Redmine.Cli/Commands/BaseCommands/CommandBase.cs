﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Services;
using Redmine.Commons;

namespace Redmine.Cli.Commands.BaseCommands {

    public abstract class CommandBase {

        [Option( CommandOptionType.NoValue, Template = "--visual", Description = "Mostrar a execução dos passos" )]
        public virtual bool Visual { get; set; }

        protected readonly UserService _userService;

        public CommandBase( ) {
            _userService = new UserService( );
            Settings.Visual = Visual;
        }

        public virtual void OnExecute( ) {
        }

        public virtual void EnsureLoged( ) {
            _userService.Login( );
        }
    }
}