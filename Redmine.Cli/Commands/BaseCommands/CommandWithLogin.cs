﻿namespace Redmine.Cli.Commands.BaseCommands {

    public abstract class CommandWithLogin: CommandBase {

        protected CommandWithLogin( ) {
            EnsureLoged( );
        }
    }
}