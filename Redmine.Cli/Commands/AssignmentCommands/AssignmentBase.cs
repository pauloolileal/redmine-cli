﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Cli.Commands.BaseCommands;

namespace Redmine.Cli.Commands.AssignmentCommands {

    [Command( "issue", "i", Description = "Comandos disponiveis para demandas" )]
    [Subcommand(
        typeof( NewAssignment ),
        typeof( UpdateAssignment ),
        typeof( SyncAssignment ),
        typeof( ListAssignment )
    )]
    public class AssignmentBase: CommandWithLogin {
    }
}