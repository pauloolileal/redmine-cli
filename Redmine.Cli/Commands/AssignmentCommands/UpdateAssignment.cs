﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Services;
using Redmine.Cli.Commands.BaseCommands;
using Redmine.Cli.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Redmine.Cli.Commands.AssignmentCommands {

    [Command( "update", "up", Description = "Atualizar uma demanda especifica" )]
    public class UpdateAssignment: CommandWithLogin {

        [Argument( 0, Name = "IssueId", Description = "Id da demanda" )]
        [Required]
        public long AssignmentId { get; set; }

        [Option( CommandOptionType.MultipleValue, Description = "Modulos do SGAP" )]
        public List<string> Modules { get; set; } = new List<string>( );

        [Option( CommandOptionType.SingleValue, Description = "Porcentagem de conclusão da demanda" )]
        public int? Percent { get; set; }

        [Option( CommandOptionType.SingleValue, Template = "-pr|--priority", Description = "Prioridade da demanda" )]
        public string Priority { get; set; }

        [Option( CommandOptionType.SingleValue, Template = "-s|--status", Description = "Situação da demanda" )]
        public string Status { get; set; }

        [Option( CommandOptionType.SingleValue, Template = "-t|--title", Description = "Titulo da demanda" )]
        public string Title { get; set; }

        [Option( CommandOptionType.SingleValue, Template = "-tp|--type", Description = "Tipo de tarefa" )]
        public string Type { get; set; }

        [Option( CommandOptionType.SingleValue, Template = "-v|--version", Description = "Versão" )]
        public string Version { get; set; }

        private readonly AssignmentService _assignmentService;

        public UpdateAssignment( ) {
            _assignmentService = new AssignmentService( );
        }

        public override void OnExecute( ) {
            var assignment = _assignmentService.Get( x => x.AssignmentId == AssignmentId );

            var project = assignment.Project;

            if ( !string.IsNullOrEmpty( Title ) )
                assignment.Title = Title;

            if ( Percent.HasValue )
                assignment.Percent = Percent.Value;

            if ( !string.IsNullOrEmpty( Priority ) ) {
                var priority = project.AllowedValues.Priority.FirstOrDefault( x => x.ToLower( ).Replace( " ", "-" ).Contains( Priority?.ToLower( ).Replace( " ", "-" ) ) );
                if ( priority == null )
                    throw new Exception( $"Essa prioridade não é aceita nesse projeto. Os valores aceitos são: {string.Join( ", ", project.AllowedValues.Priority )}" );
                else
                    assignment.Priority = priority; ;
            }

            if ( !string.IsNullOrEmpty( Status ) ) {
                var status = project.AllowedValues.Status.FirstOrDefault( x => x.ToLower( ).Replace( " ", "-" ).Contains( Status?.ToLower( ).Replace( " ", "-" ) ) );
                if ( status == null )
                    throw new Exception( $"Essa situação não é aceito nesse projeto. Os valores aceitos são: {string.Join( ", ", project.AllowedValues.Status )}" );
                else
                    assignment.Status = status;
            }

            if ( !string.IsNullOrEmpty( Type ) ) {
                var type = project.AllowedValues.AssignmentTypes.FirstOrDefault( x => x.ToLower( ).Replace( " ", "-" ).Contains( Type?.ToLower( ).Replace( " ", "-" ) ) );
                if ( type == null )
                    throw new Exception( $"Esse tipo não é aceito nesse projeto. Os valores aceitos são: {string.Join( ", ", project.AllowedValues.AssignmentTypes )}" );
                else
                    assignment.Type = type;
            }

            if ( Modules.Any( ) ) {
                if ( project.AllowedValues.Modules.Any( ) && ( !Modules.Any( ) || !project.AllowedValues.Modules.Intersect( Modules ).Any( ) ) )
                    throw new Exception( $"Esse modulo não é aceito nesse projeto. Os valores aceitos são: {string.Join( ", ", project.AllowedValues.Modules )}" );
                else
                    assignment.Modules = Modules;
            }

            if ( !string.IsNullOrEmpty( Version ) ) {
                if ( project.AllowedValues.Versions.Any( ) && !project.AllowedValues.Versions.Contains( Version ) )
                    throw new Exception( $"Essa versão não é aceito nesse projeto. Os valores aceitos são: {string.Join( ", ", project.AllowedValues.Versions )}" );
                else
                    assignment.Version = Version;
            }

            _assignmentService.Update( assignment );

            Writer.Success( "Demanda atualizada com sucesso!" );
        }
    }
}