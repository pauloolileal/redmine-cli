﻿using System.Linq;

namespace Redmine.Cli.Extensions {

    public static class StringExtensions {

        public static string TextOverflow( this string text, int count, string symbol = "..." ) {
            if ( text == null )
                return "";

            text = text.Trim( );

            if ( text.Length <= count )
                return text;

            return new string( text.Take( count - symbol.Length ).ToArray( ) ) + symbol;
        }
    }
}