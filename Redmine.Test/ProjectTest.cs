using Redmine.Application;
using Redmine.Application.Models;
using Redmine.Application.Services;
using System.Threading.Tasks;
using Xunit;

namespace Redmine.Test {

    public class ProjectTest: TestBase {

        [Fact]
        public async Task Project_must_be_addedAsync( ) {
            var service = new ProjectService( );
            var projeto = new Project {
                Description = "Projeto de Teste",
                Name = "Teste"
            };
            projeto.GenerateId( );

            service.Add( projeto );
            Browser.Close( );
        }

        [Fact]
        public async Task Project_must_be_initializedAsync( ) {
            var service = new ProjectService( );
            service.Sync( );

            Browser.Close( );
        }
    }
}